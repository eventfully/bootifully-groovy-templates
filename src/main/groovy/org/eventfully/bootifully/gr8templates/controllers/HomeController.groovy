package org.eventfully.bootifully.gr8templates.controllers

import org.springframework.boot.Banner
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.servlet.ModelAndView

@Controller
class HomeController {
  @RequestMapping("/")
  def home() {
    new ModelAndView(
        "views/home",
        [bootVersion: Banner.package.implementationVersion, 
         groovyVersion: GroovySystem.version])
  }
  
  @RequestMapping("/first")
  def first() {
    def stuffs  = ["stuff-1", "stuff-2", "stuff-3"]
    new ModelAndView(
        "views/first",
        [message: "first", stuffs: stuffs])
  }
  
  @RequestMapping("/second")
  def second() {
    new ModelAndView(
        "views/second",
        [message: "second"])
  }
  
  @RequestMapping("/help")
  def help() {
    new ModelAndView(
        "views/help",
        [message: "help"])
  }
}