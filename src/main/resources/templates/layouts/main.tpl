yieldUnescaped '<!DOCTYPE html>'
html {
  head {
	meta('http-equiv':'"Content-Type" content="text/html; charset=utf-8"')
    title(pageTitle)
    link(rel: 'stylesheet', href: '//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css')
	script(src: '//code.jquery.com/jquery-1.11.0.min.js'){}
	script(src: '//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js'){}

  }
  body {
		header(class: 'navbar navbar-static-top navbar-inverse', role: 'banner') {
			div(class: 'container'){
				div(class: 'navbar-header') {
				    button(type: 'button', class: 'navbar-toggle', 'data-toggle': 'collapse', 'data-target': '#navbar-collapse-1'){
						span(class: 'icon-bar')
						span(class: 'icon-bar')
						span(class: 'icon-bar')
					}
					a(class: 'navbar-brand', href: '/', 'Home')
				}
			
				div(class: 'collapse navbar-collapse', id: 'navbar-collapse-1', role: 'navigation') {
					ul(class: 'nav navbar-nav'){
						li {
							a(href: '/first', 'First link')
						}
						li {
							a(href: '/second', 'Second link')
						}
					}
					ul(class: 'nav navbar-nav navbar-right'){
						li() {
							a(href: '/help', 'Help')
						}						
					}
				}
			}
		}
		div(class: 'container') {
		   mainBody()
		}
		
		footer(class: 'footer') {
			div(class: 'container') {
				hr()
				p(class: 'text-center', "Copyright 2014-")
			}
		}
	
  }
}