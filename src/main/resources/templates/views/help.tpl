layout 'layouts/main.tpl',
    pageTitle: "$message",
    mainBody: contents {
      div("This is the ${message} page")
    }