Bootifully from Eventfully.org
===============================
The Bootifully project is just a playground/demo for Spring Boot
based Microservices.

Run it
----------
Gradle is bundled using the gradlew(.bat).
    gradlew bootRun
	
Package it
----------
The Gradle Application plugin creates a stand-alone distribution with start scripts.
    gradlew distZip 
	gradlew distTar
This creates a zip file with a bin and lib catalog. To include other stuff in the dist just put files under src/dist.

The code
========
The main application is the org.eventfully.bootifully.Application.java
Configuration classes goes under o.e.b.config
Camel routes goes under o.e.b.routes
There is a Spring Beans Groovy DSL so it is possible to define beans in src/main/resources/applicationContext.groovy

Frameworks used
===============

Spring Boot
-----------
The project is using Spring Boot and the newly released Spring IO Platform.

[Spring Boot](https://github.com/spring-projects/spring-boot)   
[Spring IO Platform](http://platform.spring.io/platform/)

RabbitMQ and AMQP
-----------------
https://www.rabbitmq.com/tutorials/amqp-concepts.html


Prerequisites Windows
====================

RabbitMQ
--------
Download and install
https://www.rabbitmq.com/install-windows.html
Enable http management plugin
    rabbitmq-plugins enable rabbitmq_management

Erlang
------
Download and install
http://www.erlang.org/download.html

Reference for IBM WMQ and JMS
=============================
Exchange doesn't really exist in JMS, it like a partition of queues or something.
Or like a subbroker? Is AMQP consumer based instead of queue based?

Concepts
========
Durable - persistent
Transient - non-persistent


Exchange
========

Direct exchange
---------------
Queues binds to the broker and registers a routing key. Messages are however delivered in round-robin fashion based on consumers.
It feels like the opposite of how WMQ works. The broker routes based on routing keys, so the message contains a routing key and is delivered to the next 
consumer in turn that has registered the same routing key

Fanout exchange
---------------
In a fanout the broker sends it to each and every one queue where it resides until the consumers picks it up.
There is no really good comparison with fanout in JMS although there exists fanout of topics in ActiveMQ, although it is not the same.

Headers exchange
----------------
Seems pretty useless even if it is described as "direct exchange on stereoids".
The point being that you can use it if you don't want to use a string as the routing key.

Topic exchange
--------------
Used for publish/subscribe and is the most useful use case and can be used for more or less everything.

Queues
======
Pretty much the same concept as in other messaging systems.

Bindings
========
The rules that an exchange used to route messages to queues.

Consumers
=========
Used for push, i.e. subscribing to a queue.
Consumers can share a queue but can also be declared to be exclusive.

Messages
========
Pretty much same concept as in other messaging systems.
Payload is always just a byte array.
Note that persistent messages has a performance overhead (in IBM WMQ JMS it is about a factor 30).

Connections and channels
========================
Fairly similar.

RabbitMQ extensions
===================
Per-queue message TTL
Per-message TTL
Dead-letter exchanges


Microservices checklist
=======================
According to Martin Fowler - Characteristics of a Microservice Architecture
http://martinfowler.com/articles/microservices.html

1. Componentization via Services
2. Organized around Business Capabilities
3. Products not projects
4. Smart endpoints and dumb pipes
5. Decentralized Governance
6. Decentralized Data Management
7. Infrastructure Automation
8. Design for failure
9. Evolutionary Design

LoanBroker
==========

Topics
------
loan.request.{requestId}
creditcheck.check.{applicationId}
creditcheck.result.{applicationId}
loan.rfq.{applicationId}
loan.quote.{applicationId}
